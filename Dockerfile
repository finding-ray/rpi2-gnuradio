FROM resin/raspberrypi2-debian:latest

# .gitlab-ci.yml will uncomment the following line
# when this is ran on GitLab CI in order to
# execute all commands for ARM using QEMU

#COPY qemu-arm /usr/bin/qemu-arm-static



RUN apt-get --yes update && \
    apt-get --yes install python-pip \
                          autotools-dev \
                          autoconf \
                          cmake \
                          libboost-all-dev \
                          libcppunit-dev \
                          swig \
                          doxygen \
                          liblog4cpp5 \
                          python-scipy \
                          libffi-dev \
                          libssl-dev \
                          git \
                          sudo

RUN pip install pybombs && \
    pybombs -v recipes add gr-recipes git+https://github.com/gnuradio/gr-recipes.git && \
    pybombs -v recipes add gr-etcetera git+https://github.com/gnuradio/gr-etcetera.git && \
    mkdir -p ~/prefix/

RUN apt-get --yes install libssl-dev \
                          libtiff5-dev

RUN echo "deb http://mirrors.acm.jhu.edu/debian/ jessie main contrib non-free" >> /etc/apt/sources.list && \
    echo "deb http://mirrordirector.raspbian.org/raspbian/ jessie rpi" >> /etc/apt/sources.list && \
    apt-get --yes update && \
    apt-get --yes install debian-archive-keyring && \
    apt-get --yes update && \
    apt-get --yes dist-upgrade && \
    apt-get --yes install gcc-4.9 gcc-4.9-base libgcc-4.9-dev libgcc1 gcc

RUN sed -i "51s|.*|config_opt: ' -DENABLE_DOXYGEN=$builddocs -DCMAKE_ASM_FLAGS=\"-march=armv7-a -mthumb-interwork -mfloat-abi=hard -mfpu=neon -mtune=cortex-a7\" -DCMAKE_C_FLAGS=\"-march=armv7-a -mthumb-interwork -mfloat-abi=hard -mfpu=neon -mtune=cortex-a7\"'|" ~/.pybombs/recipes/gr-recipes/gnuradio.lwr && \
    cat ~/.pybombs/recipes/gr-recipes/gnuradio.lwr

RUN apt-get --yes remove libqtgui4 libqtcore4 libqt4-xmlpatterns libqt4-xml libqt4-network libqt4-dbus && \
    apt-get --yes update && \
    apt-get --yes install libxml2-dev libxslt1-dev

RUN pybombs -v prefix init -a default ~/prefix/default/ -R gnuradio-default

RUN pybombs -v install gr-gsm && \
    pybombs -v install gr-lte && \
    pybombs -v install gr-cdma

RUN echo [grc] >> ~/.gnuradio/config.conf && \
    echo local_blocks_path=/home/username/prefix/default/src/gnuradio/grc/blocks/ >> ~/.gnuradio/config.conf
